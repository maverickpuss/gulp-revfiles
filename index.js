var gutil = require('gulp-util'),
    crypto = require('crypto'),
    fs = require('fs'),
    path = require('path'),
    through = require('through2');

/**
 *  @param Object options:
    {
        base: [optional]String  base path for rev file
        revFile: [optional]String   rev file name
    }
 */
module.exports = function (opts) {
    var options = {
            base:    ".",
            rename: false,
            revFile: "./rev-files-manifest.json"
        },
        opt;

    if (typeof(opts) === "object") {
        for (opt in opts) {
            options[opt] = opts[opt];
        }
    }

    options.revFile = path.join(options.base, options.revFile);

    return through.obj(function (file, enc, cb) {
        var curHash = null,
            curFileExt = path.extname(file.path),
            curFileRelativePath = file.path.replace(options.base, "").substr(1),
            curFileChangedRelativePath = "",
            revFileFd = null,
            originalRevInfoTxt = null,
            originalRevInfo = null,
            newRevInfoTxt = null,
            newRevInfo = {};

        if (file.isNull()) {
            this.push(file);
            return cb();
        }

        if (file.isStream()) {
            this.emit('error', new gutil.PluginError('gulp-rev-file', 
                'Streaming not supported'));
            return cb();
        }

        // prevent system cache for that file.
        try {
            revFileFd = fs.openSync(options.revFile, "rs+");
        } catch (e) {
            // create one first if not exist
            revFileFd = fs.openSync(options.revFile, "w+");

            fs.close(revFileFd);

            revFileFd = fs.openSync(options.revFile, "rs+");
        }

        originalRevInfoTxt = fs.readFileSync(options.revFile, "utf-8");

        try {
            originalRevInfo = JSON.parse(originalRevInfoTxt);
        } catch (e) {
            originalRevInfo = {};
        }

        curHash = crypto.createHash('md5').update(file.contents)
            .digest('hex').slice(0, 8);
    
        if (originalRevInfo[curFileRelativePath] &&
            originalRevInfo[curFileRelativePath].hash === curHash) {

            // this.push(file);
            return cb();
        }

        curFileChangedRelativePath = curFileRelativePath.replace(curFileExt,
                "-" + curHash + curFileExt);

        originalRevInfo[curFileRelativePath] = {
            hash: curHash,
            changedName: curFileChangedRelativePath
        };

        fs.writeSync(revFileFd, JSON.stringify(originalRevInfo), 0, "utf-8");

        if (options.rename) {
            file.path = file.path.replace(curFileExt,
                "-" + curHash + curFileExt);
        }

        fs.close(revFileFd);
        this.push(file);
        cb();
    });
};
